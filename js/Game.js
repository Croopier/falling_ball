/**
 * Created by hodek on 23.04.2017.
 */

// module aliases
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Body = Matter.Body,
    Common = Matter.Common,
    Vector = Matter.Vector;

const leftVector = Vector.create(-5, 0);
const rightVector = Vector.create(5, 0);

let mainElement = document.getElementById("game");

class Game {

    constructor(width, height, callback) {
        this.width = width;
        this.height = height;
        this.low = 0;
        this.score = 0;
        this.renderedScore=0;
        this.engine = Engine.create();
        this.render = Render.create({
            element: mainElement,
            engine: this.engine,
            options: {
                width: Math.min(document.documentElement.clientWidth, width),
                height: Math.min(document.documentElement.clientHeight, height),
                showAngleIndicator: false,
                showCollisions: false,
                wireframes: false,
                hasBounds: true,
                background: '#9fd1ff'
            }
        });
        this.ground = Bodies.rectangle(this.width / 2, this.height, this.width, 90, {
            isStatic: true,
            render: {
                visible: true,
                strokeStyle: Common.shadeColor('#434750', -20),
                fillStyle: '#434750'

            }
        });
        this.floors = [];
        this.Ball = new BallClass(width);
        this.walls = this.createWalls(height);
        this.pause = false;
        this.restart = false;
        this.endOfGame = false;
        this.move_left = false;
        this.move_right = false;
        this.Control = new Control(this.height, this.width);
        this.callback = callback;

        this.Control.renderName(this.Control.getCookie("name"));

        this.addListeners();
    }

    //start the game
    start() {
        World.add(this.engine.world, [this.Ball.getBall(), this.walls[0], this.walls[1], this.ground]);

        if(history.state.name == "") history.state.name = this.Control.getCookie("name");

        // run the engine
        Engine.run(this.engine);
        // run the renderer
        Render.run(this.render);

        this.increaseSpeed();
        this.addFloor();
        this.draw();
    }

    //mainloop function
    draw() {
        //hight enought for new floor
        if (this.low.highEnough()) {
            this.addFloor();
            this.Control.renderScore(this.score);
            this.renderedScore = Math.round(this.score)
        }

        if (this.move_left) this.Ball.move(leftVector);
        if (this.move_right) this.Ball.move(rightVector);

        //moves and removes floor if its out of bounds
        for (let i = 0; i < this.floors.length; i++) {
            this.floors[i].moveFloor();
            this.score += upVector.y * (-0.1);
            if (this.floors[i].outOfGame()) {
                this.floors[i].deleteFloor(this.engine);
                this.floors.splice(i, 1);
                i--;
            }
        }

        //check where is the ball
        if (this.Ball.outOfGame()) this.endGame();

        //if restart, delete objects
        if (this.restart) {
            if (this.floors.length > 0) {
                this.floors[0].deleteFloor(this.engine);
                this.floors.splice(0, 1);
            } else {
                this.score = 0;
                this.Control.renderScore(this.score);
                this.renderedScore = Math.round(this.score)
                this.restart = false;
                upVector.y = -1.0;
                this.addFloor();
            }
        }

        //check for pause
        if (this.pause) {
            this.Ball.toggleStatic();
        } else {
            window.requestAnimationFrame(this.draw.bind(this));
        }
    }

    //end the game
    endGame() {
        history.state.score = this.renderedScore;
        if(this.isGreater(this.renderedScore)) {
            localStorage.setItem("score"+history.state.name, history.state.score + "/" + history.state.date);
        }
        this.pause = true;
        this.endOfGame = true;
        this.Control.showEndGameModal();
    }

    //check new and old score
    isGreater(newScore){
        let old = localStorage.getItem(history.state.name);
        if(old == null) return true;
        let temp = old.split("/");
        return newScore > temp[0];
    }

    //increase speed of floors
    increaseSpeed() {
        setInterval(() => {
            if (!this.pause) {
                upVector.y -= 0.1;
            }
        }, 4000);
    }

    addFloor() {
        let floor = new Floor(this.width, this.height);
        this.floors.push(floor);
        this.low = floor;
        World.add(this.engine.world, floor.getFloor());
    }

    reset() {
        this.Ball.restartPosition();
        if (this.endOfGame) {
            this.Control.hideEndGameModal();
            this.pause = false;
            this.draw();
            this.Ball.toggleStatic();
            this.endOfGame = false;

        }
        if (this.pause == true) {
            this.pauseGame();
        }
        this.restart = true;
    }

    pauseGame() {
        if(this.endOfGame) return;
        this.pause = !this.pause;

        this.Control.togglePauseModal();
        if (this.pause == false) {
            this.draw();
            this.Ball.toggleStatic();
        }
    }

    addListeners() {
        document.body.addEventListener("keydown", (e) => {
            if (e.key === "ArrowLeft" && !this.pause) {
                e.preventDefault();
                this.move_left = true;
            }
            if (e.key === "ArrowRight" && !this.pause) {
                e.preventDefault();
                this.move_right = true;
            }
            if (e.key == "p") {
                this.pauseGame();
            }
            if (e.key == "r") {
                this.reset();
            }
        });

        document.body.addEventListener("keyup", (e) => {
            if (e.key == "ArrowLeft") {
                this.move_left = false;
            }
            if (e.key == "ArrowRight") {
                this.move_right = false;
            }
        });

        document.getElementById('play-nav').addEventListener('click', (e) => {
            switch (e.target.id) {
                case 'reset':
                    this.reset();
                    break;
                case 'pause':
                    this.pauseGame();
                    break;
                case 'show_score':

                    break;
                case 'main_page':
                    if(!this.pause)this.pauseGame();
                    this.callback();
                    break;
                default:
                    break;
            }
        });

        window.addEventListener('popstate', (e)=>{
            if(window.location.hash != "#game"){
                if(!this.pause){
                    this.pauseGame();
                }
            } else {
                if(history.state.name==""){
                    history.state.name = this.Control.getCookie("name");
                }
                this.Control.renderName(history.state.name);
            }
        });
    }

    //add walls
    createWalls(height) {
        let options = {
            isStatic: true,
            render: {
                strokeStyle: Common.shadeColor('#0003ff', -20),
                fillStyle: '#8B4513'
            }
        };
        let wallLeft = Bodies.rectangle(0, height / 2, 10, height, options);
        let wallRight = Bodies.rectangle(this.width, height / 2, 10, height, options);
        return [wallLeft, wallRight];
    }
}



