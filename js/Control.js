/**
 * Created by hodek on 24.05.2017.
 */

class Control {

    constructor(height, width) {


        //modals
        this.modal = document.getElementById("modal");
        this.pauseModal = document.getElementById('pause_modal');
        this.endGameModal = document.getElementById('endgame_modal');
        this.main = document.getElementById("game");
        this.scorePanel = document.getElementById("score");
        this.namePanel = document.getElementById("nick_name");

        //width and height
        this.height = height;
        this.width = width;

        //set gamewidth
        this.main.style.width = this.width + "px";
        this.modal.style.height = this.height + "px";

        //init
        this.renderScore(0);
    }

    togglePauseModal() {
        if (this.pauseModal.style.display === "flex") {
            this.modal.style.display = "none";
            this.pauseModal.style.display = "none";
        } else {
            this.modal.style.display = "flex";
            this.pauseModal.style.display = "flex";
        }
    }

    showEndGameModal() {
        this.modal.style.display = "flex";
        this.endGameModal.style.display = "flex";
        this.endGameModal.childNodes[5].childNodes[1].innerText = history.state.name;
        this.endGameModal.childNodes[7].childNodes[1].innerText = history.state.score;
        this.endGameModal.childNodes[9].childNodes[1].innerText = this.getPosition(history.state.name);

    }

    getPosition(name){
        let players=[];
        for (let player in localStorage) {
            if(player.substring(0,5) == "score"){
                let temp = localStorage.getItem(player).split("/");
                let user ={name: player.substring(5,player.length), score: temp[0], date: temp[1]};
                players.push(user);
            }

        }

        players.sort((a,b)=>{
            return parseInt(a.score) < parseInt(b.score);
        });

        for(let i = 0; i < players.length; i++){
            if(players[i].name == name){
                return i+1;
            }
        }
        return -1;
    }

    hideEndGameModal() {
        this.modal.style.display = "none";
        this.endGameModal.style.display = "none";

    }

    renderScore(score) {
        this.scorePanel.innerHTML = Math.round(score);
    }

    renderName(player) {
        this.namePanel.innerHTML = "<p>" + player + "</p>"
    }

    getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

}