/**
 * Created by hodek on 01.06.2017.
 */

class Main {

    constructor() {
        //main windows
        this.mainPage = document.getElementById("main");
        this.gamePage = document.getElementById("game");

        //heading
        this.header = document.querySelector('header');

        //navigation
        this.mainNav = document.getElementById('main-nav');
        this.playNav = document.getElementById('play-nav');

        //scroll listener
        this.scroll = window.addEventListener('scroll', (e) => {
            this.scrollAction(e)
        });

        //scroll Up button
        this.scrollUpButton = document.getElementById("scrollUp_button");

        //playSection
        this.playSection = document.getElementById('play');

        //play button
        this.playButtonImg = document.getElementById('play_button_img');
        this.playButton = document.getElementById('play_button');
        this.playButton.addEventListener('mouseover', (e) => this.changeImg(e));
        this.playButton.addEventListener('mouseout', (e) => this.changeImg(e));

        //game
        this.game = this.initGame();
        this.isPlaying = false;
        this.started = false;

        //form listeners
        this.gameForm = document.querySelector('form');
        this.input = this.gameForm.querySelector("input");
        this.input.addEventListener("change", (e) => this.checkInput(e));
        this.inputOK = false;
        this.gameForm.addEventListener('submit', (e) => {
            e.preventDefault();
            if (!this.inputOK) return;
            this.game.Control.renderName(e.target[0].value);
            this.player.name = e.target[0].value;
            history.pushState(this.player, "", "#game");
            this.play(e.target);
        });

        //player
        this.player = {
            name: "",
            score: 0,
            date: this.now(),
        };

        //history listener, offline api
        window.onpopstate = (e) => this.checkHistory(e);

        //after load events
        window.onload = (e) => {
            this.checkHistory(e)
            window.addEventListener('online', (e) => this.updateOnlineStatus(e));
            window.addEventListener('offline', (e) => this.updateOnlineStatus(e));

            //svg logo manipulation
            this.logo = document.getElementById("logo");
            this.svgLogo = this.logo.contentDocument;
            this.logoManipulation();
        };

        //offline indicator
        this.offlineIndicator = document.getElementById("offline_indicator");

        //scoreboard
        this.scoreTable = document.getElementById("main_score_board").querySelector("table");
        this.renderScoreTable();

        //audio control
        this.audio = document.querySelector("audio");
        this.muteButton = document.getElementById("audio_control");
        this.muteButton.addEventListener('click', (e) => this.audioControl(e));
        this.audioPlay = true;

        //resize listener
        window.addEventListener("resize", (e) => this.checkSize())
    }

    /*
     #No parameters
     returns a date with this format DD-MM-YYYY
     */
    now() {
        let d = new Date();
        let month = d.getMonth() + 1;
        let day = d.getDate();

        let output = (day < 10 ? '0' : '') + day + ". "
            + (month < 10 ? '0' : '') + month + '. '
            + d.getFullYear();

        return output;
    }

    //check the size of window, if its too small, it stops the game
    checkSize() {
        if (window.innerWidth < 600) {
            if (this.isPlaying) {
                if(!this.game.pause) {
                    this.game.pauseGame();
                }
                this.stop();
            }
            this.gameForm.childNodes[5].style.display = "block";
            this.playButton.style.display= "none";
        } else {
            this.gameForm.childNodes[5].style.display = "none";
            this.playButton.style.display= "block";
        }
    }

    //validate input of form
    checkInput() {
        let letterNumber = /^[0-9a-zA-Z]+$/;
        if ((!this.input.value.match(letterNumber)) || this.input.value.length > 8 || this.input.value.length < 1) {
            this.input.style.borderColor = 'red';
            this.inputOK = false;
        } else {
            this.input.style.borderColor = 'antiquewhite';
            this.inputOK = true;
        }
    }

    //control audio of page
    audioControl() {
        console.log(this.muteButton);
        if (this.audioPlay) {
            this.muteButton.childNodes[1].setAttribute("src", "res/audio_off.svg");
            this.audio.pause();
            this.audioPlay = false;
        } else {
            this.muteButton.childNodes[1].setAttribute("src", "res/audio_on.svg");
            this.audio.play();
            this.audioPlay = true;
        }
    }

    //update status of client
    updateOnlineStatus(e) {
        if (navigator.onLine) {
            this.offlineIndicator.style.display = "none";
        } else {
            this.offlineIndicator.style.display = "block";
        }
    }

    //change page based on history
    checkHistory(e) {
        if (window.location.hash == "#game") {
            this.play(this.gameForm);
        } else {
            this.stop();
            this.renderScoreTable();
        }
    }

    //initialize the game
    initGame() {
        if (screen.width < 600) {
            this.playButton.innerHTML = "Your device is too small, sorry :("
            this.playButton.setAttribute("disabled", true);
            return null;
        } else {
            return new Game(550, 700, this.stop.bind(this));
        }

    }

    //show srollUp button
    scrollAction(e) {
        if (window.scrollY == 0) {
            this.scrollUpButton.style.display = "none";
        } else {
            this.scrollUpButton.style.display = "block";
        }
    }

    //change img of button
    changeImg(e) {
        if (this.playButtonImg.src.slice(-1) == 'f') {
            this.playButtonImg.src = "res/running_pikachu.png";
        } else {
            this.playButtonImg.src = "res/running_pikachu.gif";
        }
    }

    //dismiss game page, show main page
    stop() {
        this.mainPage.style.display = "block";
        this.gamePage.style.display = "none";
        this.isPlaying = false;
        this.toggleHeading();
        this.changeNavigation();
    }

    //show game page, start game
    play(target) {
        this.player.name = target[0].value;
        history.state.name = this.player.name;
        if (this.player.name != "") {
            document.cookie = "name=" + this.player.name;
        }
        this.mainPage.style.display = "none";
        this.gamePage.style.display = "block";
        this.isPlaying = true;
        if (this.game == null) {
            this.gamePage.innerHTML = "<p>Sorry, your device is too small :(</p>"
        } else if (!this.started) {
            this.started = true;
            setTimeout(() => {
                this.game.start()
            }, 3000)
        }
        this.toggleHeading();
        this.changeNavigation();

    }

    //move heading out of page when playing
    toggleHeading() {
        if (this.isPlaying) {
            this.header.style.marginTop = -this.header.scrollHeight + "px";
            setTimeout(() => {
                this.header.style.position = "absolute"
            }, 3000);
        } else {
            this.header.style.marginTop = "0px";
            this.header.style.position = ""
        }
    }

    //change navigation menu when playing
    changeNavigation() {
        if (this.isPlaying) {
            this.mainNav.style.opacity = 0;
            setTimeout(() => {
                this.mainNav.style.display = 'none';
                this.playNav.style.display = 'flex';
            }, 1000);
            this.playNav.style.opacity = '1';
        } else {
            this.playNav.style.opacity = 0;
            setTimeout(() => {
                this.playNav.style.display = 'none';
                this.mainNav.style.display = 'flex';
            }, 1000);
            this.mainNav.style.opacity = '1';
        }
    }

    //render score table
    renderScoreTable() {
        let body = "<thead><tr><th>Name</th><th>Score</th>" +
            "<th>Date</th></tr></thead><tbody>";

        let players = [];
        for (let player in localStorage) {
            if (player.substring(0, 5) == "score") {
                let temp = localStorage.getItem(player).split("/");
                let user = {name: player.substring(5, player.length), score: temp[0], date: temp[1]};
                players.push(user);
            }

        }

        players.sort((a, b) => {
            return parseInt(a.score) < parseInt(b.score);
        });

        for (let i = 0; i < players.length; i++) {
            let player = "<tr><td>" + players[i].name + "</td>" +
                "<td>" + players[i].score + "</td>" +
                "<td>" + players[i].date + "</td></tr>";
            body += player;
        }

        body += "</tbody>"

        this.scoreTable.innerHTML = body;
    }

    //manipulates svg in logo
    logoManipulation() {
        this.svgLogo = this.logo.contentDocument;
        let paths = this.svgLogo.querySelectorAll("path");
        for (let i = 0; i < paths.length; i++) {
            paths[i].addEventListener("mouseover", () => {
                paths[i].style.fill = "darkslateblue";
            })
            paths[i].addEventListener("mouseout", () => {
                paths[i].style.fill = "";
            })
        }
    }
}
//initialize app
let main = new Main();

