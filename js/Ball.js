/**
 * Created by hodek on 24.05.2017.
 */

// module aliases
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Body = Matter.Body,
    Vector = Matter.Vector;


class BallClass {

    constructor(width) {
        this.ball = Bodies.circle(width / 2, 0, 24,
            {
                isStatic: false,
                render: {
                    strokeStyle: '#ffffff',
                    sprite: {
                        texture: './res/pokeball.png'
                    }
                }
            }
        );
        this.width = width;
    }

    getBall() {
        return this.ball;
    }

    move(vector) {
        Body.translate(this.ball, vector);
        Body.rotate(this.ball, vector.x/this.ball.circleRadius);
    }

    restartPosition() {
        Body.setPosition(this.ball, Vector.create(this.width / 2, 30))
    }

    toggleStatic() {
        this.ball.isStatic = !this.ball.isStatic;
    }

    outOfGame() {
        return (this.ball.position.y < -25);
    }
}