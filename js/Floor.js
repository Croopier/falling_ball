/**
 * Created by hodek on 24.05.2017.
 */
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Body = Matter.Body,
    Vector = Matter.Vector;

let upVector = Vector.create(0, -1);

class Floor {

    constructor(width, height) {
        this.height = height;
        this.parts = [];
        this.widths = [];
        this.space = width - 10;
        this.f_height = 30;
        this.gap = 100;
        this.createFloor();
    }

    getFloor() {
        return this.parts;
    }

    createFloor() {
        this.calculateWidths();
        let next = 5;
        for (let i in this.widths) {
            next += this.widths[i] / 2;
            //console.log("Adding part: " + this.widths[i] + " at " + next);
            let part = Bodies.rectangle(next, this.height+this.f_height/2, this.widths[i], this.f_height, {
                isStatic: true,
                render: {
                    strokeStyle: Common.shadeColor('#434750', -20),
                    fillStyle: '#545862'
                }
            });
            this.parts.push(part);
            next = next + this.widths[i] / 2 + this.gap;
        }
        //console.log("PARTS: " + this.parts);
    }

    moveFloor() {
        this.parts.forEach(part => {
            Body.translate(part, upVector);
        })
    }

    outOfGame(){
        return (this.parts[0].position.y + this.f_height/2  < 0);
    }

    deleteFloor(engine){
        this.parts.forEach(part => {
            World.remove(engine.world, part);
        })
    }

    highEnough(){
        return (this.parts[0].position.y < this.height-100);
    }

    calculateWidths() {
        let numOfHoles = this.getRandomInt(1, 3);
        this.space -= numOfHoles * this.gap;
        for (let i = 0; i < numOfHoles; i++) {
            let width = this.getRandomInt(1, this.space);
            this.widths.push(width);
            this.space = this.space - width;
        }
        this.widths.push(this.space);
    }

    getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

}
