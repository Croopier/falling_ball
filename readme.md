Falling Ball
===================


Tento projekt **Falling Ball** vznikl jako semestrální práce k předmětu KAJ.

Cílem projektu bylo napsat webovou aplikaci za použití nejmodernějších technologií. Semestrální práci jsem psal tak, aby splňovala co nejvíce požadavků. 

Falling Ball je hra, kde je cílem udržet kuličku (pokeball) co nejníže, protože jakmile se dostane mimo obrazovku, hra končí. Patra, kterými musí kulička propadávat se pohybují
vzhůru a zrychlují, což činí hru obtížnější s narůstajícím časem a i konečnou.

**Stránka aplikace** https://croopier.gitlab.io/falling_ball

**Stránka projektu** https://gitlab.com/Croopier/falling_ball

----------


Popis aplikace
-------------

Vyvinul jsem aplikaci, která je plně funkční na všech moderních webových prohlížečích. Aplikace bohužel nepodporuje hraní na **mobilních zařízeních**, ale je na nich funkční, tzn. nerozsype se. 
> Pro aplikaci jsem použil následující technologie:
> - **less** pro lepší a přehlednější css.
> - **Matter.js** jako herní engine pracující s **canvasem**
> - Ostatní je napsáno v nativním **javascriptu** a **html**

Při tvorbě aplikace jsem nejdříve implementoval herní logiku a posléze ostatní prvky a grafiku až naposled. Bohužel jsem s herní logikou předběhl logiku herní aplikace a proto bylo třeba některé věci měnit během implementace.

Výčet použitých technologií podle tabulky:
-----------------------------------------------------------
Technologie     | Použití
--------------- | ---
HTML |
Validita 		| Ano
Cross Browser   | Ano
Sémantické značky | Ano
Grafika SVG/Canvas| Ano - oboje
Média | Ano - hudba na pozadí
Formulářové prvky |Ano - formulář pro začátek hry
Offline aplikace | Ano, při offline je uživatel upozorněn
CSS | 
Pokročilé selektory | Ano - většinu zastupuje less
Vendor prefixy | Ano - např. některé tranformace
CSS3 transformace | Ano - např. otočení textu
CSS3 transitions/animace | Ano - posuny elementů, pulsování
 Media Queries | Ano - hlavně logo a menu, logika přes JS
 Javascript |
 OOP | Ano - celý projekt v ES6, použití **class**
 JS framewrok | Ano - matter.js 
 Pokročilá API | Ano - např. localStorage, Offline API
 Funkční historie | Ano - při posunu tam a zpět se hra pozastaví
 Offline aplikace | Ano - uživateli je oznámeno, když je offline, applikace je single page, tudíž má dostupné vše bez potřeb reloadu
 JS práce s SVG | Ano - logo v SVG, mění barvy
 Ostatní |
 Kompletnost řešení | Projekt je kompletní, dále jsou možné pouze rozšíření herní logiky
 Estetické zpracování | Aplikace je napsána v jednoduchém stylu s jemnými barvami, použité prvky z Pokemonů pro oživení

Popis fungování aplikace
------------------------
Aplikace vyžaduje od uživatele zadání jména do inputu na hlavní stránce. Poté jméno používá při hraní, dokud jej uživatel nezmění. Pokud uživatel již hrál a načetl stránku s hrou znovu, tak si aplikace pamatuje jeho poslední jméno a to použije. 
Při přepínání z hry na hlavní stránku se hra pozastavuje nebo pokud byl konec hry, je ponechána jak je a čeká na restart. 
Nejvyšší skóre je uloženo lokálně a neukládají se nižší skóre pod stejným jménem. Skóre seřazeno sestupně. 
